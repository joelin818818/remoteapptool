<meta name="google-site-verification" content="MGrMbBi28Ut57MeqKI6srDSmlrprH0lug6rwNMIi0_A" />

# RemoteApp Tool

With Microsoft RemoteApp technology, you can seamlessly use an application that is running on another computer.

RemoteApp Tool is a utility that allows you to create/manage RemoteApps hosted on Windows (7, 8, 10, XP and Server) as well as generate RDP and MSI files for clients.

If you want your RemoteApps to appear in the Start Menu of your clients, or via a web interface, check out [RAWeb](https://github.com/kimmknight/raweb)!

If you have questions, comments or suggestions about RemoteApp Tool, please visit the [forum](https://groups.google.com/forum/embed/?place=forum/remoteapptool).

## Features

* Create and manage RemoteApps on Windows desktops and servers
* Generate RDP files
* Generate MSI installers (requires WiX Toolset)
* Use a Remote Desktop Gateway
* Set options such as session timeouts
* Select icons for your apps
* File type associations for deployed apps
* Sign RDP files
* Backup RemoteApps

## Requirements

* Microsoft .Net Framework 4
* [WiX Toolset v3](https://wixtoolset.org/docs/wix3/) (If you want to create MSIs. Reboot after installing.)
* A **supported** edition of Windows XP, 7, 8, 10, or Server. See the [compatibility chart](https://gitee.com/joelin818818/remoteapptool/wikis/Windows-Compatibility).

**Note:** If you try to host RemoteApps on an incompatible edition of Windows (eg. Windows 10 Home), the tool will run but RemoteApps ***will not work***. The RDP client will appear to be connecting, then just disappear with no error shown.

## Download

**Latest:**

[RemoteApp Tool 6.0.0.0 Installer](https://github.com/kimmknight/remoteapptool/releases/download/v6.0.0.0/RemoteApp.Tool.6000.msi)

[RemoteApp Tool 6.0.0.0 Zip](https://github.com/kimmknight/remoteapptool/releases/download/v6.0.0.0/RemoteApp.Tool.6000.zip)

Please note: The latest installer no longer works on Windows XP, use the Zip instead.


## User guide

[How to create a RemoteApp and use it on another computer](https://gitee.com/joelin818818/remoteapptool/wikis/Create-a-RemoteApp-and-use-it-on-another-computer)

